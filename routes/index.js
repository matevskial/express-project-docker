var express = require('express');
var router = express.Router();

var homepage = require('../controllers/homepage');

/* GET home page. */
router.get('/', function(req, res, next) {
	homepage.item_list(req, res, next);	
});

module.exports = router;
