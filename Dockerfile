FROM node:14.15.5-alpine

WORKDIR /usr/express-project-docker/

COPY ./package.json ./
COPY ./package-lock.json ./

RUN npm install

COPY ./ ./

EXPOSE 3000

CMD ["npm", "start"]
