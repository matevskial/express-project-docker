# express-project-docker - A simple project used to test deploying with docker

This project is used to deploy with docker as a simple app for testing and learning purposes.

The app has only one route ``/``, and it lists some items from a mongo database.

With ``npm start``, a mongo database is first populated, and then the express server is started

A mongo database is meant to be deployed with another Docker container

# Ways to deploy multiple network-connected containers with Docker

## Using Docker's bridge network

* Clone this repository

* Set up the .env file
	* ``cp .env_bridge_netwok .env``

* Create a Docker bridge network
	* ``docker create network --driver bridge test_network``

* Pull ``mongo`` Docker image
	* ``docker pull mongo``

* Pull the ``node`` Docker image
	* ``docker pull node``

* Create and run a mongo container that is connected to the previously created network
	* ``docker run -d --net test_network --name test_mongo mongo``

* Change directory to this repository and run a node container using this directory as volume
	* ``cd express-project-docker``
	* ``docker run -d --net test_network --name test_node -v $(pwd):/var/www -w "/var/www" -p 3000:3000 node npm start``

* Now the node express application should work on your host machine with port 3000
	* To test it, go to ``https://localhost:3000`` in your browser, and you should be able to see the page

## Using Docker Compose

* Clone this repository

* Set up the .env file
	* ``cp .env_docker_compose .env``

* Use docker-compose to start the application
	* ``docker-compose up``

* Now the node express application should work on your host machine with port 80
	* To test it, go to ``https://localhost:3000`` in your browser, and you should be able to see the page
