var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ItemSchema = new Schema(
	{
		itemName: {type: String, required: true},
	}
)

module.exports = mongoose.model('Item', ItemSchema);
