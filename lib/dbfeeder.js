var mongoose = require('mongoose');

var Item = require('../models/item');

function itemCreate(itemName) {
	item = new Item({itemName: itemName});
	item.save(function(err) {
		if(err) {
			console.log('ERROR CREATING item: ' + item);
			return
		}
		console.log('New Item: ' + item);
	});
}

module.exports = function(mongoDBConnectionStr) {
	mongoose.connect(mongoDBConnectionStr, { useNewUrlParser: true });
	mongoose.Promise = global.Promise;

	return {
		feed : function() {
			itemCreate('docker');
			itemCreate('kubernetes');
			itemCreate('docker-compose');
			itemCreate('docker swarm');
		},

		close: function() {
			mongoose.connection.close();
		}
	}
}

//module.exports = {
	//function initialize(mongoDBConnectionStr) {
		//mongoDB = mongoDBConnectionStr;
		//mongoose.connect(mongoDB, { useNewUrlParser: true });
		//mongoose.Promise = global.Promise;
		//db = mongoose.connection;
	//}

	//function feed() {
		//itemCreate('docker');
		//itemCreate('kubernetes');
		//itemCreate('docker-compose');
		//itemCreate('docker swarm');
	//}

	//function close() {
		//db.close();
	//}
//}
