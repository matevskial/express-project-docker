module.exports = {
	host: process.env.DB_HOST || 'localhost',
	port: process.env.DB_PORT || 27017,
	datavase: 'testDB'
}
