var Item = require('../models/item');

exports.item_list = function(req, res, next) {
	Item.find({}, function(err, result) {
		if(err) return next(err);

		res.render('index', {title: 'Home Page', items: result})
	});
}
